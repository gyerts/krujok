from django.shortcuts import render
from .models import Post
from django.utils import timezone
from django.contrib.auth.decorators import login_required
from django.contrib.auth import get_user_model


@login_required
def post_list(request):
    response_data = dict()

    response_data["user"] = get_user_model().objects.get(first_name=request.user.first_name)

    response_data["posts_ready"] = Post.objects.filter(
        ready_to_read=True, finished=False).order_by('published_date')

    response_data["posts_backlog"] = Post.objects.filter(
        ready_to_read=False).order_by('published_date')

    response_data["posts_past"] = Post.objects.filter(
        ready_to_read=True, finished=True).order_by('-published_date')

    return render(request, 'krujok/post_list.html', response_data)


def register_user(request):
    from django.contrib.auth import login, authenticate
    from django.contrib.auth.forms import UserCreationForm
    from django.shortcuts import render, redirect

    print("-----------------------------------")

    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            print("[MSG][OK] form is valid, create User")
            form.save()
            first_name = form.cleaned_data.get('first_name')
            raw_password = form.cleaned_data.get('password1')
            last_name = form.cleaned_data.get('last_name')
            email = form.cleaned_data.get('email')

            user = authenticate(username=email, password=raw_password)

            login(request, user)

            print(get_user_model().objects.all())
            get_user_model().objects.get(username=email)

            user.last_name = last_name
            user.email = email
            user.username = first_name

            return redirect('home')
        else:
            print("[MSG] form errors: ")
            print(type(form.errors))
            print("[MSG][ERROR] form incorrect")
            from django.forms.utils import ErrorDict
            return render(request, 'signup.html', {"errors": form.errors})
    else:
        form = UserCreationForm()
    return render(request, 'signup.html', {'form': form})


def login_form(request):
    return render(request, 'login_form.html', {})


def login(request):
    from django.shortcuts import render, redirect
    from django.contrib.auth import authenticate, login
    email = request.GET['email']
    password = request.GET['password']

    user = authenticate(email=email, password=password)
    if user is not None:
        login(request, user)
        return redirect('home')
    else:
        print("User is none")

    return redirect('home')


def logout_view(request):
    from django.contrib.auth import logout
    from django.shortcuts import render, redirect
    logout(request)
    return redirect('home')
