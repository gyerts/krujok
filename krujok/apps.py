from django.apps import AppConfig


class KrujokConfig(AppConfig):
    name = 'krujok'
