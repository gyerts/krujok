import os
import json
from jinja2 import Environment, FileSystemLoader
import shutil

# Capture our current directory
THIS_DIR = os.path.dirname(os.path.abspath(__file__))
j2_env = Environment(loader=FileSystemLoader(os.path.join(THIS_DIR, "templates")))
j2_env.trim_blocks = True
j2_env.lstrip_blocks = True

config = None


def render_interface_template(rapan, _config):
    content = j2_env.get_template(os.path.join('code', 'interface_template.jinja2')).render({
        "rapan": rapan,
        "ip_addresses": _config.ip_addresses
    })

    output_folder = os.path.join(_config.output_folder, "static", "js", "interfaces")
    os.system("mkdir -p " + output_folder)

    with open(os.path.join(output_folder, rapan["interface"]["name"] + ".js"), "w") as fp:
        fp.write(content)


def render_django_urls_template(rapan, _config):
    content = j2_env.get_template(os.path.join('code', 'django_urls_template.jinja2')).render({
        "rapan": rapan,
        "ip_addresses": _config.ip_addresses
    })

    with open(os.path.join(_config.output_folder, "urls_gen.py"), "w") as fp:
        fp.write(content)


def render_django_views_template(_rapan, _config):
    content = j2_env.get_template(os.path.join('code', 'django_views_template.jinja2')).render({
        "rapan": _rapan,
        "ip_addresses": _config.ip_addresses
    })

    with open(os.path.join(_config.output_folder, "views_gen.py"), "w") as fp:
        fp.write(content)


def render_templates(_rapan_file_name, _config):
    with open(os.path.join(_config.interfaces_path, _rapan_file_name)) as fp:
        rapan = json.load(fp)

    render_interface_template(rapan, _config)
    render_django_urls_template(rapan, _config)
    render_django_views_template(rapan, _config)


def copy_common_files(_config):
    folder_to_copy = os.path.join(THIS_DIR, "common", "js")
    destination = os.path.join(_config.output_folder, "static", "js", "common")

    shutil.rmtree(destination, ignore_errors=True)
    shutil.copytree(folder_to_copy, destination)


def render_api(_interfaces_path, _ip_addresses, _output_folder):
    """
    Generates common client/server code for the communication purpose
    :param _interfaces_path: absolute path to api folder with *.rapan files
    :param _ip_addresses: dict with ip addresses in format {"interface_name": "127.0.0.1:8000/..."}
    :param _output_folder: should contain next folders/files ["static", "templates", "views.py", "urls.py"]
    :return: nothing, it creates files in output_folder
    """
    global config
    class Config:
        def __init__(self, _interfaces_path, _ip_addresses, _output_folder):
            self.interfaces_path = _interfaces_path
            self.ip_addresses = _ip_addresses
            self.output_folder = _output_folder

    config = Config(_interfaces_path, _ip_addresses, _output_folder)

    all_interface_files = os.listdir(config.interfaces_path)

    for file in all_interface_files:
        if file.endswith(".rapan"):
            render_templates(file, config)

    copy_common_files(config)


def get_examples(interface_name, method_name_to_show=None):
    context = {}

    with open(os.path.abspath(interface_name)) as fp:
        rapan = json.load(fp)

    if method_name_to_show:
        for method in rapan["methods"]:
            if method["name"] == method_name_to_show:
                rapan["methods"] = [method]
                break

        context["rapan"] = rapan

        path_to_template = os.path.join('view', 'main_code_generator.jinja2')
        content = j2_env.get_template(path_to_template).render(context)
    else:
        context["rapan"] = rapan
        context["config"] = config
        path_to_template = os.path.join('view', 'show_available_api.jinja2')
        content = j2_env.get_template(path_to_template).render(context)

    return content
