"""dz URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url

from external_tools.requirements_parser.views.show_requirements import show
from .views.show_generated_code import show_generated_code
from .urls_gen import urlpatterns_gen

urlpatterns = [
    url(r'^show/(?P<path_to_requirement>.*)$', show, name='show_requirements'),
    url(r'^show_usage_of_generated_code/(?P<query>.*)$', show_generated_code),
]

urlpatterns += urlpatterns_gen
