import os
from django.shortcuts import render
from external_tools.api_generator import get_examples

THIS_DIR = os.path.dirname(os.path.abspath(__file__))


def show_generated_code(request, query):
    if "/" in query:
        interface_name, method_name_to_show = query.split("/")
        content = get_examples(os.path.join(THIS_DIR, "..", "api", interface_name), method_name_to_show)
    else:
        content = get_examples(os.path.join(THIS_DIR, "..", "api", query))

    return render(request, 'examples_of_generated_api_code.html', {"generated_html_code": content})
