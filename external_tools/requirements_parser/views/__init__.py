import os
import json

from .. import controller


def greeting(_request, name, surname, age):
    return {"greeting_message": "Hello {} {}, you are {} yers old".format(name, surname, age)}


def add_must(_request, new_must_content, requirement_relative_path):
    controller.add_must(requirement_relative_path, new_must_content)
    return {}


def delete_must(_request, requirement_relative_path, must_index):
    controller.delete_must(requirement_relative_path, must_index)
    return {}
