import json

from django.shortcuts import render, redirect

from .. import controller

controller.update_all_requirements()


def show(request, path_to_requirement):
    if "" == path_to_requirement:
        return redirect("/requirements/show/main/")

    context = {}

    requirement = controller.get_requirement(path_to_requirement)
    requirement = controller.replace_tags_to_links(requirement)

    context["path_to_requirement"] = path_to_requirement
    context["requirement"] = requirement
    context["breadcrumbs"] = controller.get_breadcrumbs(path_to_requirement)
    context["namespaces"] = controller.global_namespace
    context["paths"] = controller.requirement_paths

    return render(request, '../templates/main.html', context)
