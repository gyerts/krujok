from django.db import models


class RequirementItem(models.Model):
    namespace = models.CharField(max_length=256)
    includes = models.ManyToOneRel()
    content = models.TextField()
