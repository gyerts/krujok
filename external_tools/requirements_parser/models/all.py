from django.db import models


REQUIREMENT_STATE = (
    ('OPEN', 'OPEN'),
    ('CLOSE', 'CLOSE'),
    ('IN_PROGRESS', 'IN_PROGRESS'),
)


class MustItem(models.Model):
    text = models.CharField(max_length=1024)

    def __str__(self):
        return self.text


class RequirementItem(models.Model):
    text = models.CharField(max_length=1024)
    members = models.ManyToManyField(MustItem, through='MustMembership')
    status = models.CharField(max_length=20, choices=REQUIREMENT_STATE)

    def __str__(self):
        return self.text


class RequirementGroup(models.Model):
    text = models.CharField(max_length=1024)
    members = models.ManyToManyField(RequirementItem, through='RequirementMembership')

    def __str__(self):
        return self.text


class RequirementMembership(models.Model):
    requirement = models.ForeignKey(RequirementItem, on_delete=models.CASCADE)
    requirement_group = models.ForeignKey(RequirementGroup, on_delete=models.CASCADE)


class MustMembership(models.Model):
    must = models.ForeignKey(MustItem, on_delete=models.CASCADE)
    requirement = models.ForeignKey(RequirementGroup, on_delete=models.CASCADE)
