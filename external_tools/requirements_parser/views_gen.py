from django.core.handlers.wsgi import WSGIRequest
from django.http import JsonResponse

from .views import greeting as impl_greeting
from .views import add_must as impl_add_must
from .views import delete_must as impl_delete_must


# implement this method before you can get profit
# def greeting(_request,name,surname,age):    return {"greeting_message": "string",}

def greeting(_request: WSGIRequest):
    if _request.method == 'POST' and _request.is_ajax():
        print("call registerPasport with _request.POST =", _request.POST)

        name = _request.POST.get('name', None)
        surname = _request.POST.get('surname', None)
        age = int(_request.POST.get('age'))

        try:
            data = impl_greeting(_request,name,surname,age)

            if not isinstance(data, dict):
                raise AttributeError("[ERROR]: returned data from 'greeting' is not a dict")

            if "greeting_message" not in data:
                raise AttributeError("[ERROR]: returned dict from 'greeting' must contains 'greeting_message' member")

            ans = {
                "data": data,
                "status": "SUCCESS"
            }
        except Exception as ex:
            ans = {
                "data": None,
                "status": "ERROR",
                "error_msg": str(ex)
            }

        return JsonResponse(ans)


# implement this method before you can get profit
# def add_must(_request,new_must_content,requirement_relative_path):    return {}

def add_must(_request: WSGIRequest):
    if _request.method == 'POST' and _request.is_ajax():
        print("call registerPasport with _request.POST =", _request.POST)

        new_must_content = _request.POST.get('new_must_content', None)
        requirement_relative_path = _request.POST.get('requirement_relative_path', None)

        try:
            data = impl_add_must(_request,new_must_content,requirement_relative_path)

            if not isinstance(data, dict):
                raise AttributeError("[ERROR]: returned data from 'add_must' is not a dict")

            ans = {
                "data": data,
                "status": "SUCCESS"
            }
        except Exception as ex:
            ans = {
                "data": None,
                "status": "ERROR",
                "error_msg": str(ex)
            }

        return JsonResponse(ans)


# implement this method before you can get profit
# def delete_must(_request,requirement_relative_path,must_index):    return {}

def delete_must(_request: WSGIRequest):
    if _request.method == 'POST' and _request.is_ajax():
        print("call registerPasport with _request.POST =", _request.POST)

        requirement_relative_path = _request.POST.get('requirement_relative_path', None)
        must_index = int(_request.POST.get('must_index'))

        try:
            data = impl_delete_must(_request,requirement_relative_path,must_index)

            if not isinstance(data, dict):
                raise AttributeError("[ERROR]: returned data from 'delete_must' is not a dict")

            ans = {
                "data": data,
                "status": "SUCCESS"
            }
        except Exception as ex:
            ans = {
                "data": None,
                "status": "ERROR",
                "error_msg": str(ex)
            }

        return JsonResponse(ans)


