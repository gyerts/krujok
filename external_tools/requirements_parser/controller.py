import json
import os


class Controller:
    def __init__(self, _path_to_requirement):
        self.path_to_requirement = _path_to_requirement
        self.settings = {
            "requirements_path": "requirements",
            "debug": True
        }
        self.allowed_validators = ["info", "warning", "error", "debug"]
        self.validators = {
            "namespace": "error",
            "includes": "info",
        }
        self.requirements_need_to_be_updated = True
        self.main_requirement = {}
        self.main_requirement["description"] = "lorem ipsum"
        self.main_requirement["requirements"] = {}
        self.global_namespace = {}
        self.requirement_paths = {}
        self.base_url = "/requirements/show/"

        self._parse_requirement(self.path_to_requirement)
        self.status_codes = {
            0: "open",
            1: "software design",
            2: "software construction",
            3: "software building",
            4: "software testing",
            5: "software integration",
            6: "closed"
        }

    def add_requirement(self, parent_requirement_id):
        pass

    def add_must(self, requirement_relative_path, new_must_content):
        requirement = self.get_requirement(requirement_relative_path)

        new_must = {"text": new_must_content, "status": 0}
        if "must" not in requirement:
            requirement["must"] = list()

        requirement["must"].append(new_must)
        self.save_requirement(requirement_relative_path, requirement)

    def delete_must(self, requirement_relative_path, must_index):
        print("[ENTER] to function -> Controller.delete_must('{}', '{}')".format(
            requirement_relative_path, must_index))
        requirement = self.get_requirement(requirement_relative_path)
        del requirement["must"][must_index]
        self.save_requirement(requirement_relative_path, requirement)

    def edit_requirement(self):
        pass

    def delete_requirement(self):
        pass

    @staticmethod
    def save_requirement(requirement_relative_path, requirement):
        with open(os.path.join("requirements", requirement_relative_path + ".json"), "w") as fp:
            json.dump(requirement, fp, indent=3)

    def _parse_requirement(self, path_to_requirement):
        current_dir = os.path.dirname(path_to_requirement)
        with open(path_to_requirement) as fp:
            requirement = json.load(fp)
            self.validate_requirement(requirement)

            requirement["path"] = path_to_requirement
            requirement["path"] = requirement["path"].lstrip(self.settings["requirements_path"])
            requirement["path"] = requirement["path"].lstrip(os.sep)
            requirement["path"] = requirement["path"].split(".json")[0]
            requirement["path"] = requirement["path"].replace("\\", "/")

            requirement["id"] = requirement["path"].split("/")[-1]

            if self.requirements_need_to_be_updated:
                self.add_to_global_namespaces(requirement)
                self.connect_requirement_path_with_requirement_id(requirement["namespace"], requirement, requirement["path"])

            if "includes" in requirement and requirement["includes"]:
                for include in requirement["includes"]:
                    parsed_requirement = self._parse_requirement(os.path.join(current_dir, include))

                    if "requirements" not in requirement:
                        requirement["requirements"] = dict()

                    requirement["requirements"][parsed_requirement["id"]] = parsed_requirement

        return requirement

    def update_all_requirements(self):
        path_to_file = os.path.join(self.settings["requirements_path"], "main.json")

        if self.requirements_need_to_be_updated:
            self.set_defaults()
            self.main_requirement["requirements"]["main"] = self._parse_requirement(path_to_file)
            self.requirements_need_to_be_updated = False

    def set_defaults(self):
        self.requirements_need_to_be_updated = True
        self.requirements = {}
        self.global_namespace = {}
        self.requirement_paths = {}

    def get_requirement(self, relative_path_to_requirement: str):
        print("[ENTER] to function -> Controller.get_requirement('{}')".format(
            relative_path_to_requirement))
        steps_to_requirement = relative_path_to_requirement.strip("/").split("/")

        def get_deeper_requirement(requirement, steps_to_requirement):
            print("[ENTER] to function -> Controller.get_requirement.get_deeper_requirement(\n\t'{}', \n\t'{}')".format(
                requirement, steps_to_requirement))
            if len(steps_to_requirement) == 0:
                print("found requirement: {}".format(requirement["id"]))
                return requirement

            return get_deeper_requirement(
                requirement["requirements"][steps_to_requirement[0]],
                steps_to_requirement[1:])

        requirement = get_deeper_requirement(self.main_requirement, steps_to_requirement)
        print("return requirement: {}".format(requirement["id"]))
        return requirement

    def replace_tags_to_links(self, requirement):
        if "must" in requirement:
            formatted_must = []
            for must in requirement["must"]:
                for req_id in self.requirement_paths:
                    print(req_id)
                    must["text"] = must["text"].replace("<<{}>>".format(req_id), '<a href="/requirements/show/{}">[{}]</a>'.format(
                        self.requirement_paths[req_id], req_id))
                formatted_must.append(must)
            requirement["must"] = formatted_must
        return requirement

    def get_breadcrumbs(self, path_to_requirement):
        breadcrumbs = [("home", self.base_url)]
        for part in path_to_requirement.split("/"):
            if part:
                bc = (part, self.base_url + path_to_requirement.split(part)[0] + part)
                breadcrumbs.append(bc)
        return breadcrumbs

    def add_to_global_namespaces(self, requirement):
        self.validate_requirement(requirement)

        if requirement["namespace"] not in self.global_namespace:
            self.global_namespace[requirement["namespace"]] = list()

        self.global_namespace[requirement["namespace"]].append(requirement["id"])

    def validate_requirement(self, requirement):
        def validate_param(validator, type):
            if type not in self.allowed_validators:
                raise AttributeError(
                    "Within validators {}\n'{}' has unknown type: '{}'\nThe only next validators allowed {}".format(
                        self.validators, validator, type, self.allowed_validators))

            if validator not in requirement or not requirement[validator]:
                if "info" == type:
                    print(
                        "[INFO] Next requirement has no '{}' member\n\trequirement = {}".format(validator, requirement))
                elif "warning" == type:
                    raise Warning(
                        "Next requirement has no '{}' member\nrequirement = {}".format(validator, requirement))
                elif "debug" == type:
                    if self.settings["debug"]:
                        print("[DEBUG] Next requirement has no '{}' member\nrequirement = {}".format(
                            validator, requirement))
                elif "error" == type:
                    raise AttributeError(
                        "Next requirement has no '{}' member\nrequirement = {}".format(validator, requirement))

        for validator, type in self.validators.items():
            validate_param(validator, type)
        return True

    def connect_requirement_path_with_requirement_id(self, namespace, requirement, requirement_path):
        self.validate_requirement(requirement)

        if requirement["id"] not in self.requirement_paths:
            self.requirement_paths[namespace + "." + requirement["id"]] = requirement_path
        else:
            raise AttributeError("{id}: duplication of requirement id".format(id=requirement["id"]))
