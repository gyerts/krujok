;var communication = (function() {


    //----------------------------------------------------------------------
    //                            PUBLIC METHODS
    //----------------------------------------------------------------------

    /**
     * @description: Use this method if you want to call server with params within url like:
     *                http://mysite.com?param1=value1&param2=value2
     * @param _url: requested url
     * @param _method: should be POST or GET
     * @param _json_obj: JSON in format { key: value }, with no deeper includs like {key: {key: ...}}
     * @param _callback: method which will be called after call, by xhr.onreadystatechange
     */
    function sendRequestAsUrlParams(_url, _method, _json_obj, _callback) {
        var xhr = new XMLHttpRequest();

        var params = "";
        var is_first_iteration = true;
        for(var param in _json_obj) {
            if (!is_first_iteration) {
                params += "&";
                is_first_iteration = false;
            }
            params += param+"="+encodeURIComponent(_json_obj[param]);
        }

        var method = _method.toUpperCase();
        if ("POST" == method) {
            xhr.open("POST", _url, true);
            xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
            xhr.onreadystatechange = _callback;
            xhr.send(params);
            return;
        }

        if ("GET" == method) {
            xhr.open("GET", _url+'?'+params, true);
            xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
            xhr.send(_params);
            xhr.onreadystatechange = _callback;
            xhr.send();
            return;
        }

        console.log("[ERROR]: call function sendRequestAsUrlParams with unrecognized method: " + _method);
    }

    /**
     * @description: Use this method if you want to call server with params in json format
     *
     * @param _url: requested url
     * @param _json_obj: JSON in format, use any deeper you like
     * @param _callback: method which will be called after call, by xhr.onreadystatechange
     */
    function sendRequestAsJson(_url, _json_obj, _callback) {
        var xhr = new XMLHttpRequest();
        var json = JSON.stringify(_json_obj);
        xhr.open("POST", _url, true);
        xhr.setRequestHeader('Content-type', 'application/json; charset=utf-8');
        xhr.onreadystatechange = _callback;
        xhr.send(json);
    }

    /**
     * @description: Use this method if you want to send form on server,
     *                _json_obj will be converted to the form
     *
     * @param _url: requested url
     * @param _json_obj: JSON in format { key: value }, with no deeper includs like {key: {key: ...}}
     * @param _callback: method which will be called after call, by xhr.onreadystatechange
     */
    function sendRequestAsFormOld(_url, _json_obj, _callback) {
        var boundary = String(Math.random()).slice(2);
        var boundaryMiddle = '--' + boundary + '\r\n';
        var boundaryLast = '--' + boundary + '--\r\n';

        var body = ['\r\n'];
        for (var key in _json_obj) {
          body.push('Content-Disposition: form-data; name="' + key + '"\r\n\r\n' + _json_obj[key] + '\r\n');
        }
        body = body.join(boundaryMiddle) + boundaryLast;

        var xhr = new XMLHttpRequest();
        xhr.open('POST', _url, true);
        xhr.setRequestHeader('Content-Type', 'multipart/form-data; boundary=' + boundary);
        xhr.onreadystatechange = _callback;

        xhr.send(body);
    }

    /**
     * @description: Use this method if you want to send form on server using already existing form,
     *                _json_obj will be converted to the form and added to an existing form
     *
     * @param _url: requested url
     * @param _json_obj: JSON in format { key: value }, with no deeper includs like {key: {key: ...}}
     * @param _callback: method which will be called after call, by xhr.onreadystatechange
     */
    function sendRequestAsForm(_url, _json_obj, _based_on_form_with_name, _callback) {
        var formData;
        if (_based_on_form_with_name) {
            formData = new FormData(document.forms[_based_on_form_with_name]);
        } else {
            formData = new FormData();
        }

        for (var key in _json_obj) {
            formData.append(key, _json_obj[key]);
        }
        var xhr = new XMLHttpRequest();
        xhr.open("POST", _url);
        xhr.onreadystatechange = _callback;
        xhr.send(formData);
    }

    //----------------------------------------------------------------------
    //                            PRIVATE METHODS
    //----------------------------------------------------------------------

    function _TEST_sendRequestAsUrlParams_POST() {
        var url = "http://test_communication";
        var json_obj = {"id": "ygyerts", "msg": "Hello"};
        var callback = function () {
            console.log("[INFO] sendRequestAsUrlParams success");
        };
        sendRequestAsUrlParams(url, "POST", json_obj, callback);
    }

    function _TEST_sendRequestAsUrlParams_GET() {
        var url = "http://test_communication";
        var json_obj = {"id": "ygyerts", "msg": "Hello"};
        var callback = function () {
            console.log("[INFO] sendRequestAsUrlParams success");
        };
        sendRequestAsUrlParams(url, "GET", json_obj, callback);
    }

    function _TEST_sendRequestAsJson() {
        var url = "http://test_communication";
        var json_obj = {"id": "ygyerts", "msg": "Hello"};
        var callback = function () {
            console.log("[INFO] sendRequestAsUrlParams success");
        };
        sendRequestAsJson(url, json_obj, callback);
    }

    function _TEST_sendRequestAsFormOld() {
        var url = "http://test_communication";
        var json_obj = {"id": "ygyerts", "msg": "Hello"};
        var callback = function () {
            console.log("[INFO] sendRequestAsUrlParams success");
        };
        sendRequestAsFormOld(url, json_obj, callback);
    }

    function _TEST_sendRequestAsForm() {
        var url = "http://test_communication";
        var json_obj = {"id": "ygyerts", "msg": "Hello"};
        var callback = function () {
            console.log("[INFO] sendRequestAsUrlParams success");
        };
        sendRequestAsForm(url, json_obj, "some_form_name", callback);
        sendRequestAsForm(url, json_obj, null, callback);
    }

    //----------------------------------------------------------------------
    //                            API METHODS
    //----------------------------------------------------------------------
    return {
        sendRequestAsUrlParams: sendRequestAsUrlParams,
        sendRequestAsJson: sendRequestAsJson,
        sendRequestAsFormOld: sendRequestAsFormOld,
        sendRequestAsForm: sendRequestAsForm
    }
})();
