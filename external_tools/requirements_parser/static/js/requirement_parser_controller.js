;requirement_parser_controller = (function () {
    var requirement_relative_path = NaN;

    function subscribeMustDeleteButtons() {

        $(".delete_must_item").click(function () {
            var thisElement = $(this);

            console.log(requirement_relative_path);
            var must_index_value = parseInt(thisElement.attr('data-toggle'));
            console.log(must_index_value);

            if(confirm("Want to delete?")) {
                interfaces.requirements_parser.delete_must.success = function () {
                    location.reload();
                };

                interfaces.requirements_parser.delete_must.call(
                    requirement_relative_path,
                    must_index_value
                );
            }
        });
    }

    function init(_requirement_relative_path) {
        requirement_relative_path = _requirement_relative_path;

        subscribeMustDeleteButtons();
    }
    
    return {
        init: init
    }
})();
