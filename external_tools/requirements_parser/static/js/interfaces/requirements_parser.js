interfaces = {
    requirements_parser: {
        greeting: {
            call: function (_name,_surname,_age) {
    
                var typeOf_name = typeOf(_name);
                if (typeOf_name !== "string") {
                    throw "type of '" + typeOf_name + " _name' inconsistent with required type 'string'";
                }
                var typeOf_surname = typeOf(_surname);
                if (typeOf_surname !== "string") {
                    throw "type of '" + typeOf_surname + " _surname' inconsistent with required type 'string'";
                }
                var typeOf_age = typeOf(_age);
                if (typeOf_age !== "number") {
                    throw "type of '" + typeOf_age + " _age' inconsistent with required type 'int'";
                }
    
                var request = $.ajax({
                    method: "POST",
                    url: "/requirements/greeting",
                    data: {
                        name: _name,
                        surname: _surname,
                        age: _age
                    },
                    headers: { "X-CSRFToken": getCookie('csrftoken') }
                });
    
                request.done(function(msg) {
                    console.log(msg);
                    if ("SUCCESS" == msg["status"]) {
                        if (interfaces.requirements_parser.greeting.success) {
                            interfaces.requirements_parser.greeting.success(
                                msg.data["greeting_message"]
                            );
                        }
                    } else if ("FAIL" == msg["status"]) {
                        if (interfaces.requirements_parser.greeting.fail) {
                            interfaces.requirements_parser.greeting.fail(
                                msg.data
                            );
                        }
                    } else if ("ERROR" == msg["status"]) {
                        if (interfaces.requirements_parser.greeting.error) {
                            interfaces.requirements_parser.greeting.error(
                                msg
                            );
                        }
                    } else {
                        alert("'msg[\"status\"]' has incorrect value " +
                            "(not in SUCCESS|FAIL|ERROR), the value is: " + msg["status"]);
                    }
                    
                    if (interfaces.requirements_parser.greeting.always) {
                        interfaces.requirements_parser.greeting.always(
                            msg.data["greeting_message"]
                        );
                    }
                });
                request.error(function(msg) {
                    console.log(msg);
                });            },
            success: null,
            fail: null,
            error: null,
            always: null
        },
        add_must: {
            call: function (_new_must_content,_requirement_relative_path) {
    
                var typeOf_new_must_content = typeOf(_new_must_content);
                if (typeOf_new_must_content !== "string") {
                    throw "type of '" + typeOf_new_must_content + " _new_must_content' inconsistent with required type 'string'";
                }
                var typeOf_requirement_relative_path = typeOf(_requirement_relative_path);
                if (typeOf_requirement_relative_path !== "string") {
                    throw "type of '" + typeOf_requirement_relative_path + " _requirement_relative_path' inconsistent with required type 'string'";
                }
    
                var request = $.ajax({
                    method: "POST",
                    url: "/requirements/add_must",
                    data: {
                        new_must_content: _new_must_content,
                        requirement_relative_path: _requirement_relative_path
                    },
                    headers: { "X-CSRFToken": getCookie('csrftoken') }
                });
    
                request.done(function(msg) {
                    console.log(msg);
                    if ("SUCCESS" == msg["status"]) {
                        if (interfaces.requirements_parser.add_must.success) {
                            interfaces.requirements_parser.add_must.success(
                            );
                        }
                    } else if ("FAIL" == msg["status"]) {
                        if (interfaces.requirements_parser.add_must.fail) {
                            interfaces.requirements_parser.add_must.fail(
                                msg.data
                            );
                        }
                    } else if ("ERROR" == msg["status"]) {
                        if (interfaces.requirements_parser.add_must.error) {
                            interfaces.requirements_parser.add_must.error(
                                msg
                            );
                        }
                    } else {
                        alert("'msg[\"status\"]' has incorrect value " +
                            "(not in SUCCESS|FAIL|ERROR), the value is: " + msg["status"]);
                    }
                    
                    if (interfaces.requirements_parser.add_must.always) {
                        interfaces.requirements_parser.add_must.always(
                        );
                    }
                });
                request.error(function(msg) {
                    console.log(msg);
                });            },
            success: null,
            fail: null,
            error: null,
            always: null
        },
        delete_must: {
            call: function (_requirement_relative_path,_must_index) {
    
                var typeOf_requirement_relative_path = typeOf(_requirement_relative_path);
                if (typeOf_requirement_relative_path !== "string") {
                    throw "type of '" + typeOf_requirement_relative_path + " _requirement_relative_path' inconsistent with required type 'string'";
                }
                var typeOf_must_index = typeOf(_must_index);
                if (typeOf_must_index !== "number") {
                    throw "type of '" + typeOf_must_index + " _must_index' inconsistent with required type 'int'";
                }
    
                var request = $.ajax({
                    method: "POST",
                    url: "/requirements/delete_must",
                    data: {
                        requirement_relative_path: _requirement_relative_path,
                        must_index: _must_index
                    },
                    headers: { "X-CSRFToken": getCookie('csrftoken') }
                });
    
                request.done(function(msg) {
                    console.log(msg);
                    if ("SUCCESS" == msg["status"]) {
                        if (interfaces.requirements_parser.delete_must.success) {
                            interfaces.requirements_parser.delete_must.success(
                            );
                        }
                    } else if ("FAIL" == msg["status"]) {
                        if (interfaces.requirements_parser.delete_must.fail) {
                            interfaces.requirements_parser.delete_must.fail(
                                msg.data
                            );
                        }
                    } else if ("ERROR" == msg["status"]) {
                        if (interfaces.requirements_parser.delete_must.error) {
                            interfaces.requirements_parser.delete_must.error(
                                msg
                            );
                        }
                    } else {
                        alert("'msg[\"status\"]' has incorrect value " +
                            "(not in SUCCESS|FAIL|ERROR), the value is: " + msg["status"]);
                    }
                    
                    if (interfaces.requirements_parser.delete_must.always) {
                        interfaces.requirements_parser.delete_must.always(
                        );
                    }
                });
                request.error(function(msg) {
                    console.log(msg);
                });            },
            success: null,
            fail: null,
            error: null,
            always: null
        },
    }
};