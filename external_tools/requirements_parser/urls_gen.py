from django.conf.urls import url
from .views_gen import greeting
from .views_gen import add_must
from .views_gen import delete_must

urlpatterns_gen = [
    url(r'^greeting', greeting),
    url(r'^add_must', add_must),
    url(r'^delete_must', delete_must),
]