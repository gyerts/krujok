import os
from external_tools.api_generator import render_api
from .controller import Controller

THIS_DIR = os.path.dirname(os.path.abspath(__file__))

render_api(
    os.path.join(THIS_DIR, "api"),
    {"requirements_parser": "/requirements"},
    os.path.join(THIS_DIR)
)

controller = Controller("/home/ygyerts/PycharmProjects/dz/requirements/main.json")
