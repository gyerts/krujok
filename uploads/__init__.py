"""
How to integrate thos APP

================================================
1) add to settings.py file

    MEDIA_URL = '/media/'
    MEDIA_ROOT = os.path.join(BASE_DIR, 'media')

================================================
2) add to settings.py file

    INSTALLED_APPS = [
        ...
        "upload_file"
    ]

================================================
3) add to urls.py file

    from django.conf.urls import url, include
    urlpatterns = [
        ...
        url(r'^uploads/', include("upload_file.urls")),
    ]


    from django.conf import settings
    from django.conf.urls.static import static
    if settings.DEBUG:
        urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
"""
