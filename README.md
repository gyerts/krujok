# Simple File Upload Example

Example used in the blog post [How to Upload Files With Django](https://simpleisbetterthancomplex.com/tutorial/2016/08/01/how-to-upload-files-with-django.html)

## Running Locally

```bash
git clone
```

```bash
pip3 install -r requirements.txt
```

```bash
python3 manage.py migrate
```

```bash
python3 manage.py runserver
```


```
Requirement should be written in next format
{
  "id": "main",
  "namespace": "main",
  "includes": [
  ],
  "must": [
    "have this <<namespace.requirement_id>>",
    "and this <<namespace.requirement_id>> should also have",
  ]
}
```