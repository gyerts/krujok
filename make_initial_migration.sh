rm db.sqlite3_temp
mv db.sqlite3 db.sqlite3_temp

rm -rf accounts/migrations
rm -rf krujok/migrations
rm -rf uploads/migrations


python3 manage.py makemigrations accounts
python3 manage.py makemigrations krujok
python3 manage.py makemigrations uploads

python3 manage.py migrate

python3 manage.py createsuperuser
